

# Nx Demo

This project was generated using [Nx](https://nx.dev).

<p style="text-align: center;"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png" width="450"></p>

## Feature
Simple todo app. Todo list viewable in web app and mobile app by consuming api from backend (fastify). Todos can only be added via the web app and the list can be viewed on the web and mobile app.
## Run this workspace

- Install all package need before
  - `pnpm install`
- You can serve development apps API (Fastify) and Web (React)
  - `pnpm nx run-many --target=serve --projects=api,web`
- You can also run mobile apps (flutter)
  - Move to directory `apps/mobile/` and run `flutter run`

